package ru.nsu.ccfit.legostaeva.focusstart.parser;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Этот класс содержит результат парсинга входных данных. В него входят:
 * <ul>
 * <li>Порядок сортировки</li>
 * <li>Тип сортируемых данных</li>
 * <li>Выходной файл</li>
 * <li>Сортируемые файлы/файл</li>
 * </ul>
 */
public class ArgumentsParsingResult {
    private boolean isSortingAsc = true;
    private boolean isStringValue;
    private BufferedWriter bufferedWriter;
    private List<FileInputStream> inputStreams = new ArrayList<>();
    private List<String> inputFileNames = new ArrayList<>();

    void addInputFileName(String curInputFileName) {
        inputFileNames.add(curInputFileName);
    }

    void addInputStream(FileInputStream curInputFile) {
        inputStreams.add(curInputFile);
    }

    public boolean isSortingAsc() {
        return isSortingAsc;
    }

    void setSortingAsc(boolean sortingAsc) {
        isSortingAsc = sortingAsc;
    }

    public boolean isStringValue() {
        return isStringValue;
    }

    void setStringValue(boolean stringValue) {
        isStringValue = stringValue;
    }

    public BufferedWriter getBufferedWriter() {
        return bufferedWriter;
    }

    void setBufferedWriter(BufferedWriter bufferedWriter) {
        this.bufferedWriter = bufferedWriter;
    }

    public List<String> getInputFileNames() {
        return inputFileNames;
    }

    public List<FileInputStream> getInputStreams() {
        return inputStreams;
    }
}
