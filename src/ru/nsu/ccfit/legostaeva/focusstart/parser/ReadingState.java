package ru.nsu.ccfit.legostaeva.focusstart.parser;

/**
 * Это класс-перечисление входных данных. С его помощью {@link ru.nsu.ccfit.legostaeva.focusstart.parser.ArgumentsParser}
 * определяет, с чем он работает в данный момент.
 */

public enum ReadingState {
    SORTING,
    VALUE,
    OUTPUT_FILE_NAME,
    INPUT_FILE_NAMES
}
