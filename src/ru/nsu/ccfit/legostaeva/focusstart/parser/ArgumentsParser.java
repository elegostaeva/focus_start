package ru.nsu.ccfit.legostaeva.focusstart.parser;

import ru.nsu.ccfit.legostaeva.focusstart.exception.arguments.ArgumentsFormatException;
import ru.nsu.ccfit.legostaeva.focusstart.exception.arguments.NotEnoughArgumentsException;
import ru.nsu.ccfit.legostaeva.focusstart.exception.output.FileNotEmptyException;

import java.io.*;
import java.util.Scanner;

/**
 * Этот класс парсит входные данные, которые содержат:
 * <ul>
 * <li>Порядок сортировки (не обязательный аргумент)</li>
 * <li>Тип сортируемых данных</li>
 * <li>Выходной файл</li>
 * <li>Сортируемые файлы/файл</li>
 * </ul>
 */

public class ArgumentsParser {

    private static final String DESCENDING_ORDER_ARGUMENT = "-d";
    private static final String ASCENDING_ORDER_ARGUMENT = "-a";
    private static final String STRING_VALUE_ARGUMENT = "-s";
    private static final String INTEGER_VALUE_ARGUMENT = "-i";

    private ReadingState currentReadingState = ReadingState.SORTING;

    public ArgumentsParsingResult parseArguments(String[] args) throws NotEnoughArgumentsException, ArgumentsFormatException, FileNotFoundException, FileNotEmptyException {
        if (args.length < 3) {
            throw new NotEnoughArgumentsException("Not enough arguments.");
        }

        ArgumentsParsingResult parsingResult = new ArgumentsParsingResult();
        for (String argument : args) {
            switch (currentReadingState) {
                case SORTING:
                    if (DESCENDING_ORDER_ARGUMENT.equals(argument)) {
                        parsingResult.setSortingAsc(false);
                        currentReadingState = ReadingState.VALUE;
                        break;
                    } else if (ASCENDING_ORDER_ARGUMENT.equals(argument)) {
                        currentReadingState = ReadingState.VALUE;
                        break;
                    } else {
                        currentReadingState = ReadingState.VALUE;
                    }
                case VALUE:
                    if (STRING_VALUE_ARGUMENT.equals(argument)) {
                        parsingResult.setStringValue(true);
                        currentReadingState = ReadingState.OUTPUT_FILE_NAME;
                        break;
                    } else if (INTEGER_VALUE_ARGUMENT.equals(argument)) {
                        currentReadingState = ReadingState.OUTPUT_FILE_NAME;
                        break;
                    } else {
                        throw new ArgumentsFormatException("Wrong argument. Expected -i or -s.");
                    }
                case OUTPUT_FILE_NAME:
                    try {
                        File outputFile = new File(argument);
                        if (outputFile.length() != 0) {
                            System.out.print("Do you want to clean " + argument + " ? Answer y/n: ");
                            Scanner in = new Scanner(System.in);
                            String answer = in.nextLine();
                            if ("n".equals(answer)) {
                                in.close();
                                throw new FileNotEmptyException("Output file is not empty.");
                            }
                        }
                        parsingResult.setBufferedWriter(new BufferedWriter(new FileWriter(outputFile)));
                        currentReadingState = ReadingState.INPUT_FILE_NAMES;
                        break;
                    } catch (IOException e) {
                        System.out.println("Can't open file " + argument);
                    }
                case INPUT_FILE_NAMES:
                    File inputFile = new File(argument);
                    if (!inputFile.exists()) {
                        System.out.println("File " + argument + " doesn't exist.");
                        break;
                    } else if (!inputFile.canRead()) {
                        System.out.println("Can't read file " + argument);
                        break;
                    } else if (inputFile.length() == 0) {
                        System.out.println("File " + argument + " is empty.");
                        break;
                    }
                    parsingResult.addInputFileName(argument);
                    FileInputStream curInputFile = new FileInputStream(inputFile);
                    parsingResult.addInputStream(curInputFile);
            }
        }
        return parsingResult;
    }
}
