package ru.nsu.ccfit.legostaeva.focusstart;

import ru.nsu.ccfit.legostaeva.focusstart.exception.arguments.ArgumentsFormatException;
import ru.nsu.ccfit.legostaeva.focusstart.exception.arguments.NotEnoughArgumentsException;
import ru.nsu.ccfit.legostaeva.focusstart.exception.output.FileNotEmptyException;
import ru.nsu.ccfit.legostaeva.focusstart.parser.ArgumentsParser;
import ru.nsu.ccfit.legostaeva.focusstart.parser.ArgumentsParsingResult;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        ArgumentsParser argumentsParser = new ArgumentsParser();
        ArgumentsParsingResult argumentsParsingResult = new ArgumentsParsingResult();
        try {
            argumentsParsingResult = argumentsParser.parseArguments(args);
            MergeSorter mergeSorter = new MergeSorter();
            mergeSorter.sort(argumentsParsingResult);
            argumentsParsingResult.getBufferedWriter().close();
        } catch (NotEnoughArgumentsException exception) {
            System.out.println(exception.getMessage());
            aboutHandler();
        } catch (ArgumentsFormatException | FileNotEmptyException exception) {
            System.out.println(exception.getMessage());
        } catch (FileNotFoundException exception) {
            System.out.println(exception.getMessage());
            for (InputStream curStream : argumentsParsingResult.getInputStreams()) {
                curStream.close();
            }
        } catch (NullPointerException exception) {
            System.out.println("The input files do not have the correct data.");
        }
    }

    private static void aboutHandler() {
        try (BufferedReader in = new BufferedReader(new FileReader("./resources/about.txt"))) {
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}