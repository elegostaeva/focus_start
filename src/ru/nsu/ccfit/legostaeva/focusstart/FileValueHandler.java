package ru.nsu.ccfit.legostaeva.focusstart;

/**
 * Объекты этого класса хранят текущие значения(строку или целое число) из входных файлов.
 */

class FileValueHandler<T extends Comparable<T>> {
    private T value;
    private Integer fileIndex;

    FileValueHandler(T value, Integer fileIndex) {
        this.value = value;
        this.fileIndex = fileIndex;
    }

    T getValue() {
        return value;
    }

    Integer getFileIndex() {
        return fileIndex;
    }
}
