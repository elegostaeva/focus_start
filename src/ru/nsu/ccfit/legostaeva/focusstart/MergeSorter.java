package ru.nsu.ccfit.legostaeva.focusstart;

import ru.nsu.ccfit.legostaeva.focusstart.parser.ArgumentsParsingResult;

import java.io.IOException;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Этот класс осуществляет сортировку слиянием при помощи вызова метода {@link #sort(ArgumentsParsingResult)}
 */

class MergeSorter {
    void sort(ArgumentsParsingResult argumentsParsingResult) throws IOException {
        FileValueComparator fileValueComparator = new FileValueComparator(argumentsParsingResult.isSortingAsc());
        PriorityQueue<FileValueHandler> priorityQueue = new PriorityQueue<>(fileValueComparator);

        List<Scanner> scanners = argumentsParsingResult.getInputStreams().stream()
                .map(Scanner::new)
                .collect(Collectors.toList());

        for (int i = 0; i < scanners.size(); i++) {
            if (argumentsParsingResult.isStringValue()) {
                if (scanners.get(i).hasNextLine()) {
                    priorityQueue.add(new FileValueHandler(scanners.get(i).nextLine(), i));
                } else checkFileEmpty(argumentsParsingResult, scanners, i);
            } else {
                if (scanners.get(i).hasNextInt()) {
                    priorityQueue.add(new FileValueHandler(scanners.get(i).nextInt(), i));
                } else {
                    checkFileEmpty(argumentsParsingResult, scanners, i);
                }
            }
        }

        boolean sortingIsOver = false;

        while (!sortingIsOver) {
            FileValueHandler currentHandler = priorityQueue.poll();

            argumentsParsingResult.getBufferedWriter().write(currentHandler.getValue().toString() + "\r\n");

            Integer currentFileIndex = currentHandler.getFileIndex();

            if (argumentsParsingResult.isStringValue()) {
                if (scanners.get(currentFileIndex).hasNextLine()) {
                    FileValueHandler nextHandler = new FileValueHandler(scanners.get(currentFileIndex).nextLine(), currentFileIndex);
                    checkFileOrder(
                            fileValueComparator,
                            currentHandler,
                            nextHandler,
                            argumentsParsingResult.getInputFileNames().get(currentFileIndex),
                            scanners.get(currentFileIndex),
                            priorityQueue
                    );
                } else checkFileEmpty(argumentsParsingResult, scanners, currentFileIndex);
            } else {
                if (scanners.get(currentFileIndex).hasNextInt()) {
                    FileValueHandler nextHandler = new FileValueHandler(scanners.get(currentFileIndex).nextInt(), currentFileIndex);
                    checkFileOrder(
                            fileValueComparator,
                            currentHandler,
                            nextHandler,
                            argumentsParsingResult.getInputFileNames().get(currentFileIndex),
                            scanners.get(currentFileIndex),
                            priorityQueue
                    );
                } else checkFileEmpty(argumentsParsingResult, scanners, currentFileIndex);
            }
            if (priorityQueue.isEmpty()) {
                sortingIsOver = true;
            }


        }
    }

    private void checkFileEmpty(ArgumentsParsingResult argumentsParsingResult, List<Scanner> scanners, int i) {
        if (scanners.get(i).hasNext()) {
            System.out.println("Incorrect input in file " + argumentsParsingResult.getInputFileNames().get(i));
            scanners.get(i).close();
        } else {
            scanners.get(i).close();
        }
    }

    private void checkFileOrder(FileValueComparator fileValueComparator,
                                FileValueHandler currentHandler,
                                FileValueHandler nextHandler,
                                String filename,
                                Scanner scanner,
                                PriorityQueue<FileValueHandler> priorityQueue
    ) {
        if (fileValueComparator.compare(currentHandler, nextHandler) > 0) {
            System.out.println("Program can't sort the file " + filename + " because the data isn't in the correct order");
            scanner.close();
        } else {
            priorityQueue.add(nextHandler);
        }
    }
}
