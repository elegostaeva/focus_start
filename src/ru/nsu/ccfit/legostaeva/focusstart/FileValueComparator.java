package ru.nsu.ccfit.legostaeva.focusstart;

import java.util.Comparator;

/**
 * Этот класс устанавливает правила сравнения экземпляров класса FileValueHandler,
 * переопределив метод {@link Comparator#compare(Object, Object)}.
 */

public class FileValueComparator implements Comparator<FileValueHandler> {
    private final boolean isAscendingOrder;

    FileValueComparator(boolean isAscendingOrder) {
        this.isAscendingOrder = isAscendingOrder;
    }

    @Override
    public int compare(FileValueHandler o1, FileValueHandler o2) {
        int compareResult = o1.getValue().compareTo(o2.getValue());
        if (!isAscendingOrder) {
            compareResult = -compareResult;
        }
        return compareResult;
    }
}
