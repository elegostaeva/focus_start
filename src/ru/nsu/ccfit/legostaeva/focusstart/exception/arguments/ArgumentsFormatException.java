package ru.nsu.ccfit.legostaeva.focusstart.exception.arguments;

public class ArgumentsFormatException extends Exception {
    public ArgumentsFormatException(String message) {
        super(message);
    }
}
