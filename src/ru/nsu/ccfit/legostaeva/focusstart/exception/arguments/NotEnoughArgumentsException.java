package ru.nsu.ccfit.legostaeva.focusstart.exception.arguments;

public class NotEnoughArgumentsException extends Exception {
    public NotEnoughArgumentsException(String message) {
        super(message);
    }
}
