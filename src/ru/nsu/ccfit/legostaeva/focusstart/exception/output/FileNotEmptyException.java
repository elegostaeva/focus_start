package ru.nsu.ccfit.legostaeva.focusstart.exception.output;

public class FileNotEmptyException extends Exception{
    public FileNotEmptyException(String message) {
        super(message);
    }
}
